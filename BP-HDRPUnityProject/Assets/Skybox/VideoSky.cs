/**
 * @file VideoSky.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Settings object for video sky shader.
 */

using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[VolumeComponentMenu("Sky/VideoSky")]
[SkyUniqueID(NEW_SKY_UNIQUE_ID)]
public class VideoSky : SkySettings
{
    const int NEW_SKY_UNIQUE_ID = 20382390;

    [Tooltip("Specify the cubemap HDRP uses to render the sky.")]
    public TextureParameter hdriSky = new TextureParameter(null);

    public override Type GetSkyRendererType()
    {
        return typeof(VideoSkyRenderer);
    }

    public override int GetHashCode()
    {
        int hash = base.GetHashCode();
        unchecked
        {
            hash = hdriSky.value != null ? hash * 23 + hdriSky.GetHashCode() : hash;
        }
        return hash;
    }
}

