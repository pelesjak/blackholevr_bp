/**
 * @file BHApprox.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script connecting approximation visualization of black hole to render pipeline.
 */

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System;

[Serializable, VolumeComponentMenu("Post-processing/Custom/BHApprox")]
public sealed class BHApprox : CustomPostProcessVolumeComponent, IPostProcessComponent
{
    public BoolParameter activate = new BoolParameter(false);
    public FloatParameter alpha = new FloatParameter(1.77f);
    public FloatParameter omega = new FloatParameter(1.45f);

    public TextureParameter view360 = new TextureParameter(null);

    Material m_Material;

    public bool IsActive() => m_Material != null && activate.value;

    // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > HDRP Default Settings).
    public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

    const string kShaderName = "Hidden/Shader/BHApprox";

    public override void Setup()
    {
        if (Shader.Find(kShaderName) != null)
            m_Material = new Material(Shader.Find(kShaderName));
        else
            Debug.LogError($"Unable to find shader '{kShaderName}'. Post Process Volume BHApprox is unable to load.");
    }

    public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
    {
        if (m_Material == null)
            return;

        m_Material.SetFloat("_Alpha", alpha.value);
        m_Material.SetFloat("_Omega", omega.value);

        m_Material.SetTexture("_View360", view360.value);
        m_Material.SetTexture("_InputTexture", source);
        HDUtils.DrawFullScreen(cmd, m_Material, destination);
    }

    public override void Cleanup()
    {
        CoreUtils.Destroy(m_Material);
    }
}
