/**
 * @file BHEffect.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script connecting pre-generated data visualization of black hole to render pipeline.
 */

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System;

[Serializable, VolumeComponentMenu("Post-processing/Custom/BHEffect")]
public sealed class BHEffect : CustomPostProcessVolumeComponent, IPostProcessComponent
{
    public BoolParameter activate = new BoolParameter(false);

    public ClampedFloatParameter edgeBlurWidth = new ClampedFloatParameter(0.05f, 0, 1);
    public Vector3Parameter positionBH = new Vector3Parameter(Vector3.zero);
    public TextureParameter view360 = new TextureParameter(null);
    public TextureParameter data = new TextureParameter(null);
    public IntParameter numR = new IntParameter(0);
    public IntParameter Rindex = new IntParameter(0);
    public FloatParameter Rratio = new FloatParameter(0);
    public FloatParameter catchAngle = new FloatParameter(0);
    public BoolParameter view360IsStereo = new BoolParameter(false);

    Material m_Material;

    public bool IsActive() => m_Material != null && activate.value;

    // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > HDRP Default Settings).
    public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

    const string kShaderName = "Hidden/Shader/BHEffect";

    public override void Setup()
    {
        if (Shader.Find(kShaderName) != null)
            m_Material = new Material(Shader.Find(kShaderName));
        else
            UnityEngine.Debug.LogError($"Unable to find shader '{kShaderName}'. Post Process Volume BHEffect is unable to load.");
    }

    public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
    {
        if (m_Material == null)
            return;

        m_Material.SetVector("_BHPosition", positionBH.value);
        m_Material.SetFloat("_NumR", numR.value);
        m_Material.SetInt("_RIndex", Rindex.value);
        m_Material.SetFloat("_RRatio", Rratio.value);
        m_Material.SetFloat("_PixelH", 1.0f / (numR.value * numR.value));
        m_Material.SetFloat("_EdgeBlurWidth", edgeBlurWidth.value);
        m_Material.SetFloat("_CatchAngle", catchAngle.value);
        m_Material.SetTexture("_View360", view360.value);
        m_Material.SetTexture("_BHData", data.value);
        m_Material.SetInt("_View360IsStereo", view360IsStereo.value ? 1 : 0);
        m_Material.SetTexture("_InputTexture", source);
        HDUtils.DrawFullScreen(cmd, m_Material, destination);
    }

    public override void Cleanup()
    {
        CoreUtils.Destroy(m_Material);
    }
}