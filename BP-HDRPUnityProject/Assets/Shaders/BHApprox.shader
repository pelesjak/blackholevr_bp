/**
 * @file BHApprox.shader
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Shader to vizulize black hole using approximation function.
 */

Shader "Hidden/Shader/BHApprox"
{
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
    };

    Varyings Vert(Attributes input)
    {
        Varyings output;
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
        return output;
    }

    /*
        Creates rotation matrix for given axis.
    */
    float3x3 RotationMat(float angle, float3 axis)
    {
        float c, s;
        sincos(angle, s, c);

        float t = 1 - c;
        float x = axis.x;
        float y = axis.y;
        float z = axis.z;

        return float3x3(
            t * x * x + c, t * x * y - s * z, t * x * z + s * y,
            t * x * y + s * z, t * y * y + c, t * y * z - s * x,
            t * x * z - s * y, t * y * z + s * x, t * z * z + c
            );
    }

    /*
        Transforms point from viewport coordinates to camera coordinates.
    */
    float4 viewportToCamera(float2 uv, float depth) {

        float2 uvClip = uv * 2.0 - 1.0;
        float4 clipPos = float4(uvClip, depth, 1.0);
        float4x4 projM = _XRInvProjMatrix[unity_StereoEyeIndex];
        projM[1][1] *= -1;
        float4 viewPos = mul(projM, clipPos);  // inverse projection by clip position
        viewPos /= viewPos.w; // perspective division

        return viewPos;
    }

    /*
        Transforms point from viewport coordinates to world coordinates.
    */
    float4 viewportToWorld(float2 uv, float depth) {

        float4 viewPos = viewportToCamera(uv, depth);
        float4 worldPos = mul(_XRInvViewMatrix[unity_StereoEyeIndex], viewPos);
        return worldPos + _XRWorldSpaceCameraPosViewOffset[unity_StereoEyeIndex];
    }

    /*
        Transforms point from world coordinates to viewport coordinates.
    */
    float3 WorldToViewport(float3 worldPos) {
        float4 world4 = float4(worldPos, 1) - _XRWorldSpaceCameraPosViewOffset[unity_StereoEyeIndex];
        float4 viewPos = mul(_XRViewMatrix[unity_StereoEyeIndex], world4);
        float4x4 projM = _XRProjMatrix[unity_StereoEyeIndex];
        projM[1][1] *= -1;
        float4 clip4 = mul(projM, viewPos);
        float3 clipPos = clip4.xyz / -clip4.w;
        float2 uv = (-clipPos.xy + 1) / 2;
        return float3(uv, -clip4.w);
    }

    /*
        Black hole approximation function.
    */
    float CalculateBHApproxDistortion(float R, float R_0, float alpha, float omega) {
        return R_0 / R - (1.0 / (R_0 - alpha)) * (((R - R_0) * (2.0 * R + R_0)) / pow((R - omega),2));
    }

    // List of properties

    /*
    * Parameters for approximation function.
    */
    float _Alpha;
    float _Omega;

    /*
    * Normal view from camera.
    */
    TEXTURE2D_X(_InputTexture);

    /*
    * 360 view from camera.
    */
    TEXTURE2D(_View360);

    SamplerState Sampler_Repeat_Linear;

    float4 BHApprox(Varyings i) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

        uint2 positionSS = i.texcoord * _ScreenSize.xy;
        float3 mainColor = LOAD_TEXTURE2D_X(_InputTexture, positionSS).rgb;
        float3 bhColor = 0.0;
        float blendValue = 1.0;

        float3 cameraPos = _XRWorldSpaceCameraPos[unity_StereoEyeIndex].xyz;
        float depth = LoadCameraDepth(i.texcoord * _ScreenSize.xy);

        float3 pixWorld = viewportToWorld(i.texcoord, 0).xyz;
        // Get points in world coords
        float3 pixCamera = pixWorld - cameraPos;
        float3 bhCamera = -cameraPos;

        // Get point angle to camera and black hole.
        float pixAngleCos = dot(pixCamera, bhCamera) / (length(pixCamera) * length(bhCamera));
        float pixAngle = acos(pixAngleCos);

        float3 pixWorldReal = viewportToWorld(i.texcoord, depth).xyz;
        // Don't render black hole if is behind user or point is to close to user
        if (pixAngle < PI / 2 && length(pixWorldReal) > 1) {
            // Get distortion axis.
            float3 axis = normalize(cross(pixCamera, bhCamera));
            // distance from black hole - black hole is located at (0,0,0) - 
            float r = length(cameraPos);
            // limit r to 2 for avoiding negative square root
            r = r < 2.1 ? 2.1 : r;
            // After a lot of simplification
            float b = sin(pixAngle) * r / sqrt(1 - 2.0 / r);
            //if (b < sqrt(32)) return 0;
            // Distance of photon at closest point to black hole
            float r_0 = 2 * b / pow(3 * sqrt(3), 1.0 / 3) * cos(acos(-3 * sqrt(3) / b) / 3);

            
            if (b < sqrt(27))return 0;
            
            // Get distortion angle calculated from nearest point as a cosinus
            float cosPhi = CalculateBHApproxDistortion(r, r_0, _Alpha, _Omega);

            // Change
            float distortionAngle = acos(cosPhi) * 2 - PI;

            if (distortionAngle >= 0 || b < sqrt(32)) {
                blendValue = 0;
                //float3 rotationPoint = cameraPos + normalize(pixCamera) * pixAngleCos * r;

                // Rotate point by distortion angle around calculated axis.
                float3 pixDist = mul(RotationMat(distortionAngle, axis), pixWorld );

                float2 uv = WorldToViewport(pixDist).xy;
                depth = LoadCameraDepth(uv * _ScreenSize.xy);
                float3 newPixWorld = viewportToWorld(uv, depth).xyz;
                
                // if final point is very close to camera, get color from 360 view
                bool use360View = length(newPixWorld) < 1;

                if (!use360View && uv.x <= 1 && uv.y <= 1 && uv.x >= 0 && uv.y >= 0) {
                    // get new color from normal camera view
                    positionSS = uv * _ScreenSize.xy;
                    bhColor = LOAD_TEXTURE2D_X(_InputTexture, positionSS).xyz;
                }
                else {
                    // transform to spherical coords 
                    float lon = atan2(pixDist.x, pixDist.z);
                    float lat = asin(pixDist.y / length(pixDist));

                    // get new color from 360 camera view
                    uv = float2((lon + PI) / (2 * PI), (lat + PI / 2) / PI);

                    bhColor = SAMPLE_TEXTURE2D(_View360, Sampler_Repeat_Linear, uv).rgb;
                }
            }
        }
        
        float3 outColor = lerp(bhColor, mainColor, blendValue);
        return float4(outColor, 1);
    }

    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "BHApprox"

            ZWrite Off
            ZTest Always
            Blend Off
            Cull Off

            HLSLPROGRAM
                #pragma fragment BHApprox
                #pragma vertex Vert
            ENDHLSL
        }
    }
    Fallback Off
}
