/**
 * @file BHEffect.shader
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Shader to vizulize black hole using approximation function.
 */

Shader "Hidden/Shader/BHEffect"
{
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
    };

    Varyings Vert(Attributes input)
    {
        Varyings output;
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
        return output;
    }

    /*
        Creates rotation matrix for given axis.
    */
    float3x3 RotationMat(float angle, float3 axis)
    {
        float c, s;
        sincos(angle, s, c);

        float t = 1 - c;
        float x = axis.x;
        float y = axis.y;
        float z = axis.z;

        return float3x3(
            t * x * x + c, t * x * y - s * z, t * x * z + s * y,
            t * x * y + s * z, t * y * y + c, t * y * z - s * x,
            t * x * z - s * y, t * y * z + s * x, t * z * z + c
            );
    }

    /*
        Transforms point from viewport coordinates to camera coordinates.
    */
    float4 viewportToCamera(float2 uv, float depth) {

        float2 uvClip = uv * 2.0 - 1.0;
        float4 clipPos = float4(uvClip, depth, 1.0); 
        float4x4 projM = _XRInvProjMatrix[unity_StereoEyeIndex];
        projM[1][1] *= -1;
        float4 viewPos = mul(projM, clipPos);  // inverse projection by clip position
        viewPos /= viewPos.w; // perspective division

        return viewPos;
    }

    /*
        Transforms point from viewport coordinates to world coordinates.
    */
    float4 viewportToWorld(float2 uv, float depth) {

        float4 viewPos = viewportToCamera(uv, depth);
        float4 worldPos = mul(_XRInvViewMatrix[unity_StereoEyeIndex], viewPos);
        return worldPos;
    }

    /*
        Transforms point from world coordinates to viewport coordinates.
    */
    float3 WorldToViewport(float3 worldPos) {
        float4 world4 = float4(worldPos, 1) - _XRWorldSpaceCameraPosViewOffset[unity_StereoEyeIndex];
        float4 viewPos = mul(_XRViewMatrix[unity_StereoEyeIndex], world4);
        float4x4 projM = _XRProjMatrix[unity_StereoEyeIndex];
        projM[1][1] *= -1;
        float4 clip4 = mul(projM, viewPos);
        float3 clipPos = clip4.xyz / -clip4.w;
        float2 uv = (-clipPos.xy + 1) / 2;
        return float3(uv, -clip4.w);
    }

    float LinearEyeDepth2(float rawdepth)
    {
        return 1.0 / (_ZBufferParams.z * rawdepth + _ZBufferParams.w);
    }

    float DepthTo01(float depth)
    {
        return 9.44962 * log(31.6355 * depth - 79.9654) - 29.5686;
    }

    float3 ToGrid(float3 vec) 
    {
        return pow(abs(cos(vec * PI * 4)), 20);
    }

    // List of properties to control your post process effect
    /*
    * Normal view from camera.
    */
    TEXTURE2D_X(_InputTexture);

    /*
    * 360 view from camera.
    */
    TEXTURE2D(_View360);

    /*
    * Black hole distortion data texture.
    */
    TEXTURE2D(_BHData);

    /*
    * Number of different distances in data texture.
    */
    int _NumR;

    /*
    * Flag for using stereo 360 view.
    */
    bool _View360IsStereo;

    /*
    * Height of a pixel.
    */
    float _PixelH;

    /*
    * Blur width in range <0;1>.
    */
    float _EdgeBlurWidth;

    /*
    * Current index in data texture.
    */
    int _RIndex;

    /*
    * Ratio between current distance and next distance - used for interpolation.
    */
    float _RRatio;

    /*
    * Angle under which photon trajectory ends on event horizont.
    */
    float _CatchAngle;

    /*
    * Position of black hole in world coords.
    */
    float3 _BHPosition;
    SamplerState Sampler_Repeat_Linear;
    SamplerState Sampler_MirrorU_ClampV_Point;

    float4 BHEffect(Varyings i) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

        uint2 positionSS = i.texcoord * _ScreenSize.xy;
        float3 mainColor = LOAD_TEXTURE2D_X(_InputTexture, positionSS).rgb;
        float3 bhColor = 0.0;
        float blendValue = 1.0;

        
        float3 cameraPos = _XRWorldSpaceCameraPos[unity_StereoEyeIndex].xyz;
        float depth = LoadCameraDepth(i.texcoord * _ScreenSize.xy);
        // Get points in world coords
        float3 pixWorld = viewportToWorld(i.texcoord, 0).xyz;
        float3 bhWorld = 0;
        float3 pixCamera = pixWorld - cameraPos;
        float3 bhCamera = bhWorld - cameraPos;

        // Get point angle to camera and black hole.
        float angle = acos(dot(pixCamera, bhCamera) / (length(pixCamera) * length(bhCamera)));
        float angleValue = -((2 * angle) / PI - 1);

        float3 pixCameraReal = viewportToCamera(i.texcoord, depth).xyz;
        // Don't render black hole if is behind user or point is to close to user
        if (angleValue > 0 && length(pixCameraReal) > 1) {

            // blur edge of visualization
            blendValue = 0;
            if (angleValue < _EdgeBlurWidth) {
                blendValue = 1 - angleValue / _EdgeBlurWidth;
            }
            // get distortion data
            float pixCoord1 = _RIndex * _NumR - 1; //+ pixDistIndex;
            float pixCoord2 = _RIndex > 0 ? (_RIndex < _NumR ? (_RIndex - 1) * _NumR : (_NumR - 1) * _NumR): 0;
            pixCoord2 -= 1;
            float2 data1 = SAMPLE_TEXTURE2D(_BHData, Sampler_MirrorU_ClampV_Point, float2(angleValue, _PixelH * pixCoord1)).rg;
            float2 data2 = SAMPLE_TEXTURE2D(_BHData, Sampler_MirrorU_ClampV_Point, float2(angleValue, _PixelH * pixCoord2)).rg;
            float2 data = lerp(data2, data1, _RRatio);
            // photon trajectory ends on event horizont
            if (data.y == 0 || angle <= _CatchAngle) {
                bhColor = 0;
            }
            else {
                // positive if distortion did not crossed axis user/blackhole
                float phi = asin(data.x * 2 - 1);
                
                float distortionAngle = angle + phi;

                // distortion axis
                float3 axis = normalize(cross(pixCamera, bhCamera));
                float3 pixDist = mul(RotationMat(distortionAngle, axis), pixWorld);

                float2 uv = WorldToViewport(pixDist).xy;
                depth = LoadCameraDepth(uv * _ScreenSize.xy);
                float3 newPixWorld = viewportToWorld(uv, depth).xyz;
                // if final point is very close to camera, get color from 360 view
                bool use360View = length(newPixWorld) < 1;

                if (!use360View && uv.x <= 1 && uv.y <= 1 && uv.x >= 0 && uv.y >= 0) {
                    // get new color from normal camera view
                    bhColor = SAMPLE_TEXTURE2D_X(_InputTexture, Sampler_Repeat_Linear, uv).rgb;
                }
                else {
                    // transform to spherical coords 
                    float lon = atan2(pixDist.x, pixDist.z);
                    float lat = asin(pixDist.y / length(pixDist));

                    // get new color from 360 camera view
                    float2 uv = float2((lon + PI) / (2 * PI), (lat + PI / 2) / PI);

                    bhColor = SAMPLE_TEXTURE2D(_View360, Sampler_Repeat_Linear, uv).rgb;
                }
            }
        }

        float3 outColor = lerp(bhColor, mainColor, blendValue);
        return float4(outColor, 1);
    }

    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "BHEffect"

            ZWrite Off
            ZTest Always
            Blend Off
            Cull Off

            HLSLPROGRAM
                #pragma fragment BHEffect
                #pragma vertex Vert
            ENDHLSL
        }
    }
    Fallback Off
}


