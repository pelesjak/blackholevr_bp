﻿/**
 * @file Camera360.cs
 * @author Jakub Peleška
 * @date 17 May 2021
 * @brief Script for getting 360 view texture.
 */

using UnityEngine;

public class Camera360 : MonoBehaviour
{
    /*
     * Target render texture for 360 view.
     */
    public RenderTexture equirectTexture;

    /*
     * Size of one face of cubemap.
     */
    public int cubeMapSize;

    /*
     * Flag for rendering one face per frame or all faces per frame.
     */
    public bool oneFacePerFrame;

    /*
     * Flag for rendering each eye separetly.
     */
    public bool renderStereo;

    RenderTexture cubeMap;
    RenderTexture cubeMapLeft;
    RenderTexture cubeMapRight;
    public GameObject cameraObject;
    Camera cam;

    void Start()
    {
        LateUpdate();
    }
    void LateUpdate()
    {
        if (oneFacePerFrame)
        {
            var faceToRender = Time.frameCount % 6;
            var faceMask = 1 << faceToRender;
            UpdateCubemap(faceMask);
        }
        else
        {
            UpdateCubemap(63); // all six faces
        }
    }


    void UpdateCubemap(int faceMask)
    {
        if (cameraObject && !cam)
        {
            cam = cameraObject.GetComponent<Camera>();
            cam.farClipPlane = 100; // don't render very far into cubemap
            cam.enabled = false;
            cameraObject.transform.rotation = Quaternion.identity;
        }

        if (!cubeMap)
        {
            cubeMap = new RenderTexture(cubeMapSize, cubeMapSize,0);
            cubeMap.dimension = UnityEngine.Rendering.TextureDimension.Cube;
            cubeMap.hideFlags = HideFlags.HideAndDontSave;
            //GetComponent<Renderer>().sharedMaterial.SetTexture("_Cube", cubeMap);
        }

        if (!cubeMapLeft && !cubeMapRight)
        {
            cubeMapLeft = new RenderTexture(cubeMapSize, cubeMapSize, 0);
            cubeMapLeft.dimension = UnityEngine.Rendering.TextureDimension.Cube;
            cubeMapLeft.hideFlags = HideFlags.HideAndDontSave;
            cubeMapRight = new RenderTexture(cubeMapSize, cubeMapSize, 0);
            cubeMapRight.dimension = UnityEngine.Rendering.TextureDimension.Cube;
            cubeMapRight.hideFlags = HideFlags.HideAndDontSave;
            //GetComponent<Renderer>().sharedMaterial.SetTexture("_Cube", cubeMap);
        }

        if (cameraObject && cam && cubeMap)
        {
            if (renderStereo)
            {
                cam.RenderToCubemap(cubeMapLeft, faceMask, Camera.MonoOrStereoscopicEye.Left);
                cam.RenderToCubemap(cubeMapRight, faceMask, Camera.MonoOrStereoscopicEye.Right);
                cubeMapLeft.ConvertToEquirect(equirectTexture, Camera.MonoOrStereoscopicEye.Left);
                cubeMapRight.ConvertToEquirect(equirectTexture, Camera.MonoOrStereoscopicEye.Right);
            }
            else
            {
                cam.RenderToCubemap(cubeMap, faceMask, Camera.MonoOrStereoscopicEye.Mono);
                cubeMap.ConvertToEquirect(equirectTexture, Camera.MonoOrStereoscopicEye.Mono);
            }
        }
        
    }
}
