/**
 * @file BHManager.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script for managing the vizualization of the black hole.
 *
 * Script for managing the vizualization of the black hole.
 * Contains all the neccesary parameters to visulize black hole.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class BHManager : MonoBehaviour
{
    /*
      * Black hole object in the scene.
      */
    public GameObject blackHole;
    /*
     * Object of user's head in the scene.
     */
    public GameObject headXR;
    /* 
     * Main camera object.
     */
    public Camera cam;
    /*
     * Path to black hole data.
     */
    public String dataPath;
    /*
     * Saved texture of black hole data.
     */
    public Texture2D BHData;
    public VolumeProfile postProcessProfile;
    /*
     * Data of current scene state.
     */
    public AppData AppState;
    /* 
     * Camera for getting 360 view from user's position.
     */
    public Camera360 camera360;

    private float Ratio;
    private int numR;
    private BHEffect bhEffect;
    private BHApprox bhApprox;
    private List<float> rRange = new List<float>();
    private List<float> inBHAngles = new List<float>();

    private float BHDistance { get { return (cam.transform.position - blackHole.transform.position).magnitude; } }
    void Start()
    {
        postProcessProfile.TryGet(out bhEffect);
        postProcessProfile.TryGet(out bhApprox);
        // Loading of distance parameters
        if (bhEffect != null && dataPath != null)
        {
            BytesConverter converter = GetComponent<BytesConverter>();
            float[] range, catchAngles;
            converter.BytesArraysToTex(dataPath, out range, out numR, out catchAngles);
            inBHAngles = catchAngles.ToList();
            inBHAngles.Sort();
            rRange = new List<float>(range);
            bhEffect.numR.Override(numR);
        }
        // Map distance parameters to given range
        rRange = rRange.Select(x => Remap(x, 2.3f, 20, 0.0f, 25)).ToList();
        // BHEffect2.rs = rRange.Select(r => new FloatParameter { value = r }).ToArray();

    }

    // Update is called once per frame
    void Update()
    {
        if (bhEffect != null)
        {
            // Get current distance parameter.
            if (rRange.Count > 0)
            {
                int index = rRange.BinarySearch(BHDistance);
                index = index >= 0 ? Mathf.Clamp(index, 0, rRange.Count - 1) : Mathf.Clamp(-index - 1, 0, rRange.Count - 1);
                int beforeIndex = index - 1 < 0 ? 0 : index - 1;

                Ratio = (BHDistance - rRange[beforeIndex]) / (rRange[index] - rRange[beforeIndex]);

                float catchAngle = Mathf.Lerp(inBHAngles[inBHAngles.Count - beforeIndex], inBHAngles[inBHAngles.Count - index], Ratio);

                bhEffect.catchAngle.Override(catchAngle);
                bhEffect.Rindex.Override(index);
                bhEffect.Rratio.Override(Ratio);
            }
            // Set blackhole postion
            bhEffect.positionBH.Override(blackHole.transform.position);
            bhEffect.view360IsStereo.Override(camera360.renderStereo);
        }
    }

    /*
     * Disable black hole visualization on entering pause menu.
     */
    public void OnPause()
    {
        AppState.BHApproxEnabled = bhApprox.activate.value;
        AppState.BHEffectEnabled = bhEffect.activate.value;
        bhApprox.activate.Override(false);
        bhEffect.activate.Override(false);
    }

    /*
     * Activate black hole visualization on leaving pause menu.
     */
    public void OnResume()
    {
        bhApprox.activate.Override(AppState.BHApproxEnabled);
        bhEffect.activate.Override(AppState.BHEffectEnabled);
    }

    private float Remap(float value, float fromMin, float fromMax, float toMin, float toMax)
    {
        return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
    }

}
