using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAlongEllipse : MonoBehaviour
{
    public GameObject target;
    public Vector3 center = Vector3.zero;
    public float semiMajorAxis = 1;
    public float semiMinorAxis = 1;
    public float inclination = 45;
    public float speed = 1;
    public int resolution = 1000;

    private Transform _targetTransform;
    private Vector3[] positions;
    private float _angle;
    private Quaternion _inclination { get => Quaternion.AngleAxis(inclination, Vector3.right); }
    void Start()
    {
        _angle = 0;
        target.SetActive(true);
        _targetTransform = target.transform;
    }

    void Update()
    {
        _angle += Time.deltaTime * speed;
        _targetTransform.position = GetPointOnEllipse(_angle);
        _targetTransform.forward = GetTangentToEllipse(_angle);
    }

    void OnDrawGizmosSelected()
    {
        positions = CreateEllipse();
        for (int i = 0; i < resolution; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(positions[i], positions[i+1]);
        }
    }

    public Vector3[] CreateEllipse()
    {
        Vector3[] positions = new Vector3[resolution + 1];
        for (int i = 0; i <= resolution; i++)
        {
            float angle = (float)i / resolution * 2.0f * Mathf.PI;
            positions[i] = GetPointOnEllipse(angle);
        }

        return positions;
    }

    public Vector3 GetPointOnEllipse(float angle)
    {
        Vector3 ret = new Vector3(semiMajorAxis * Mathf.Cos(angle), 0, semiMinorAxis * Mathf.Sin(angle));
        return _inclination * ret + center;
    }

    public Vector3 GetTangentToEllipse(float angle)
    {
        Vector3 ret = new Vector3(-semiMajorAxis * Mathf.Sin(angle), 0, semiMinorAxis * Mathf.Cos(angle));
        return _inclination * ret;
    }
}
