﻿/**
 * @file BytesConverter.cs
 * @author Jakub Peleška
 * @date 17 May 2021
 * @brief Script to convert bytes data to texture form.
 */

using System;
using System.IO;
using UnityEngine;


/*
 * Old version of converter.
 */
public class BytesConverter : MonoBehaviour
{
    public Texture2D BytesMapToTex(string file)
    {
        if (File.Exists(file))
        {
            int width, height;
            byte[] byteMap;
            float[] floatMap;
            Color[] colorMap;
            using (BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open)))
            {
                width = reader.ReadInt32();
                height = reader.ReadInt32();
                byteMap = reader.ReadBytes(width * height * 3 * sizeof(float));
                floatMap = new float[width * height * 3];
                Buffer.BlockCopy(byteMap, 0, floatMap, 0, width * height * sizeof(float) * 3);
            }
            colorMap = new Color[width * height];
            for (int i = 0; i < height * width; i++)
            {
                double lat = floatMap[i * 3];
                double lon = floatMap[i * 3 + 1];
                double g = floatMap[i * 3 + 2];
                double xx = Math.Cos(lat) * Math.Sin(lon);
                double yy = Math.Sin(lat);
                double zz = Math.Cos(lat) * Math.Cos(lon);

                // gnomonic projection
                double u = xx / zz / Math.Tan(70.0f / 180.0f * Math.PI);
                double v = yy / zz / Math.Tan(70.0f / 180.0f * Math.PI);
                if ((u > 1.0) || (u < -1.0) || (v > 1.0) || (v < -1.0)) g = 0.0f;

                u = (u + 1.0f) / 2.0f;
                v = (v + 1.0f) / 2.0f;

                colorMap[i] = new Color((float)u, (float)v, (float)g);
            }
            Texture2D tex = new Texture2D(width, height, TextureFormat.RGBAFloat, false, true);
            tex.SetPixels(colorMap);
            return tex;
        }
        return null;
    }


    /*
     * New version of converter.
     */
    public Texture2D BytesArraysToTex(string file, out float[] Rs, out int num_R, out float[] catchAngles)
    {
        if (File.Exists(file))
        {
            int length;
            byte[] byteRs, byteData;
            float[] data;
            Color[] colorData;
            using (BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open)))
            {
                length = reader.ReadInt32();
                num_R = reader.ReadInt32();
                byteRs = reader.ReadBytes(num_R * sizeof(float));
                byteData = reader.ReadBytes(num_R * num_R * length * sizeof(float)*2);
                Rs = new float[num_R];
                data = new float[num_R * num_R * length*2];
                Buffer.BlockCopy(byteRs, 0, Rs, 0, num_R * sizeof(float));
                Buffer.BlockCopy(byteData, 0, data, 0, num_R * num_R * length * sizeof(float) *2);
            }
            colorData = new Color[num_R * num_R * length];

            int currentIndex = 0;
            float currentAngle = 0;
            catchAngles = new float[num_R];
            for (int i = 0; i < num_R * num_R * length; i++)
            {
                float phi = data[i * 2]/2;
                float g = data[i * 2 + 1];

                if((i / length) % num_R == 0)
                {
                    if (i > 0 && g == 0 && data[(i-1) * 2 + 1] == 1 && i / (num_R * length) != currentIndex)
                    {
                        currentIndex = i / (num_R * length);
                        currentAngle = (Mathf.PI / 2)  * ((float)(i % length) / (float)(length - 1));
                        catchAngles[currentIndex - 1] = currentAngle;
                    }
                }
                colorData[i] = new Color(phi, g, 0);
            }
            catchAngles[num_R-1] = Mathf.PI / 2;
            Texture2D tex = new Texture2D(length, num_R * num_R, TextureFormat.RGFloat, false, true);
            tex.SetPixels(colorData);
            return tex;
        }
        num_R = 0;
        Rs = null;
        catchAngles = null;
        return null;
    }

    /*
     * Converts old version of data to PNG texture.
     */
    public void BytesMapToPNG(string file)
    {
        Texture2D tex = BytesMapToTex(file);
        if (tex != null) File.WriteAllBytes(Path.ChangeExtension(file, "png"), tex.EncodeToPNG());
    }

    /*
     * Converts old version of data to EXR texture.
     */
    public void BytesMapToEXR(string file)
    {
        Texture2D tex = BytesMapToTex(file);
        if (tex != null) File.WriteAllBytes(Path.ChangeExtension(file, "exr"), tex.EncodeToEXR(Texture2D.EXRFlags.OutputAsFloat));
    }

    /*
     * Converts new version of data to EXR texture.
     */
    public void BytesDataToEXR(string file)
    {
        float[] Rs, catchAngles;
        int numR;
        Texture2D tex = BytesArraysToTex(file, out Rs, out numR, out catchAngles);
        if (tex != null) File.WriteAllBytes(Path.ChangeExtension(file, "exr"), tex.EncodeToEXR(Texture2D.EXRFlags.OutputAsFloat));
    }

    /*
     * Converts new version of data to PNG texture.
     */
    public void BytesDataToPNG(string file)
    {
        float[] Rs, catchAngles;
        int numR;
        Texture2D tex = BytesArraysToTex(file, out Rs, out numR, out catchAngles);
        if (tex != null) File.WriteAllBytes(Path.ChangeExtension(file, "png"), tex.EncodeToPNG());
    }
}
