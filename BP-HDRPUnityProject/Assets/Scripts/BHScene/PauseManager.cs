/**
 * @file PauseManager.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script to handle pause menu.
 */

using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class PauseManager : MonoBehaviour
{
    /*
     * Pause menu UI object.
     */
    public GameObject pauseMenuCanvas;

    public BHManager blackHoleManager;
    public MultiuserManager multiuserManager;

    /*
     * Current scene state.
     */
    public AppData AppState;

    private bool paused = false;
    private bool wasPressed = false;
    private XRRayInteractor[] rayIndicators;
    

    private void Start()
    {
        rayIndicators = FindObjectsOfType<XRRayInteractor>();
        UpdatePauseMenu();
    }

    void Update()
    {
        // Check if button for menu was pushed.
        var controllers = new List<InputDevice>();
        var desiredCharacteristics = InputDeviceCharacteristics.HeldInHand | InputDeviceCharacteristics.Controller | InputDeviceCharacteristics.Right;
        InputDevices.GetDevicesWithCharacteristics(desiredCharacteristics, controllers);
        foreach (InputDevice device in controllers)
        {
            bool pressed = false;
            device.IsPressed(InputHelpers.Button.Grip, out pressed);

            if (wasPressed && !pressed)
            {
                OnToggle();
            }
            wasPressed = pressed;
        }
    }

    /*
     * Show/Hide pause menu.
     */
    public void UpdatePauseMenu()
    {
        Time.timeScale = paused ? 0 : 1;
        pauseMenuCanvas.SetActive(paused);
        foreach (XRRayInteractor indicator in rayIndicators)
        {
            indicator.enabled = paused;
        }
    }

    public void OnToggle()
    {
        paused = !paused;
        UpdatePauseMenu();
        if (paused) blackHoleManager.OnPause();
        else blackHoleManager.OnResume();
    }

    /*
     * On Main menu button.
     */
    public void OnMainMenu()
    {
        OnToggle();
        if (PhotonNetwork.IsConnected) multiuserManager.LeaveRoom();
        else multiuserManager.OnLeftRoom();
    }

    /*
     * On black hole effect button.
     */
    public void OnShowBHEffect()
    {
        AppState.BHApproxEnabled = false;
        AppState.BHEffectEnabled = true;
        OnToggle();
    }

    /*
     * On black hole approximation button.
     */
    public void OnShowBHApprox()
    {
        AppState.BHApproxEnabled = true;
        AppState.BHEffectEnabled = false;
        OnToggle();
    }

    /*
     * On disable visualization button.
     */
    public void OnNoEffect()
    {
        AppState.BHApproxEnabled = false;
        AppState.BHEffectEnabled = false;
        OnToggle();
    }
}