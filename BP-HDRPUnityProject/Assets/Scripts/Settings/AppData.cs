/**
 * @file AppData.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Data structure for app state.
 */

using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AppData", order = 2)]
public class AppData : ScriptableObject
{
    public enum skyData
    {
        staticSpace,
        dynamicSpace,
        beach
    }

    public bool BHEffectEnabled = true;
    public bool BHApproxEnabled = false;
    public skyData SkySetting = skyData.staticSpace; 
}
