/**
 * @file SettingsData.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Data structure for app settings.
 */
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SettingsData", order = 1)]
public class SettingsData : ScriptableObject
{
    public int UserHeight = 180;
}
