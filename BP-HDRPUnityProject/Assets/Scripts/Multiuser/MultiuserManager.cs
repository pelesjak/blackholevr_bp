﻿/**
 * @file MultiuserManager.cs
 * @author Jakub Peleška
 * @date 17 May 2021
 * @brief Script to manage multiuser connection in main scene.
 */

using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit;

public class MultiuserManager : MonoBehaviourPunCallbacks
{
    public GameObject ParentObject;
    public GameObject UserRepresentation;
    public GameObject BlackHole;
    public float StartDistanceFromBH = 5;

    void Start()
    {

        //Create user representation
        Transform xrRig = FindObjectOfType<XRRig>().transform;
        float floorOffset = xrRig.position.y;
        Vector3 startPostion;
        Quaternion startRotation;
        int index = PhotonNetwork.IsConnected ? PhotonNetwork.CurrentRoom.PlayerCount - 1 : 0;
        GetSpawntransform(index, StartDistanceFromBH, BlackHole.transform.position, floorOffset, out startPostion, out startRotation);

        xrRig.position = startPostion;
        xrRig.rotation = startRotation;

        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Instantiate(UserRepresentation.name, new Vector3(0, 0, 0), Quaternion.identity, 0);
        }
        else
        {
            Instantiate(UserRepresentation );
        }
        
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }


    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting


        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

        }
    }


    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects


        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
        }
    }

    /*
     * Get users spawn transform.
     */
    public void GetSpawntransform(int playerNumber, float radius, Vector3 center, float floorOffset, out Vector3 position, out Quaternion rotation)
    {
        int maxNumber = PhotonNetwork.IsConnected ? PhotonNetwork.CurrentRoom.MaxPlayers : 1;

        playerNumber %= maxNumber;

        float angle = 2 * Mathf.PI / maxNumber * playerNumber;

        position = new Vector3(radius * Mathf.Sin(angle), floorOffset, radius * Mathf.Cos(angle));

        rotation = Quaternion.Euler(0,180-angle,0);

        position += new Vector3(center.x,0,center.z);
    }
}
