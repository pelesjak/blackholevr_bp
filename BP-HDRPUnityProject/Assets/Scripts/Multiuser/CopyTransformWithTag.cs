/**
 * @file CopyTransformWithTag.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script to copy transform of objects with given tag - used for multiuser connection.
 */

using Photon.Pun;
using UnityEngine;

public class CopyTransformWithTag : MonoBehaviour
{
    public string Tag;
    public PhotonView view;
    private Transform model;
    void Start()
    {
        model = GameObject.FindGameObjectWithTag(Tag).transform;
    }

    void Update()
    {
        if(!PhotonNetwork.IsConnected || view.IsMine)
        {
            transform.position = model.position;
            transform.rotation = model.rotation;
        }
    }
}
