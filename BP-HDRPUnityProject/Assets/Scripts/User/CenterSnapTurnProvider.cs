using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using System;

public class CenterSnapTurnProvider : MonoBehaviour
{
    public enum InputAxes
    {
        Primary2DAxis = 0,
        Secondary2DAxis = 1,
    }
    public GameObject center;
    [Range(0.01f, 1)]
    public float inputThreshold = 0.8f;
    [Range(5, 90)]
    public float stepAngle = 30;
    [Range(0.1f, 2)]
    public float stepDelaySeconds = 0.5f;
    public InputAxes inputBinding = InputAxes.Primary2DAxis;

    private List<InputDevice> _inputDevices = new List<InputDevice>();
    private float _holdLeftTime = 0;
    private float _holdRightTime = 0;

    void Update()
    {
        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right | InputDeviceCharacteristics.HeldInHand, _inputDevices);
        foreach(InputDevice device in _inputDevices)
        {
            bool left, right;
            if(inputBinding == InputAxes.Primary2DAxis)
            {
                InputHelpers.IsPressed(device, InputHelpers.Button.PrimaryAxis2DLeft, out left, inputThreshold);
                InputHelpers.IsPressed(device, InputHelpers.Button.PrimaryAxis2DRight, out right, inputThreshold);
            } 
            else
            {
                InputHelpers.IsPressed(device, InputHelpers.Button.SecondaryAxis2DLeft, out left, inputThreshold);
                InputHelpers.IsPressed(device, InputHelpers.Button.SecondaryAxis2DRight, out right, inputThreshold);
            }
            

            if (left && (_holdLeftTime == 0 || _holdLeftTime + stepDelaySeconds <= Time.time))
            {
                _turnAroundPointByAngle(-stepAngle);
                _holdLeftTime = Time.time;
            }
            else if (!left && _holdLeftTime != 0)
            {
                _holdLeftTime = 0;
            }

            if (right && (_holdRightTime == 0 || _holdRightTime + stepDelaySeconds <= Time.time))
            {
                _turnAroundPointByAngle(stepAngle);
                _holdRightTime = Time.time;

            }
            else if (!right && _holdRightTime != 0)
            {
                _holdRightTime = 0;
            }
        }
    }

    private void _turnAroundPointByAngle(float angle)
    {
        float radAngle = Mathf.Deg2Rad * angle;
        Vector3 pos = transform.position;
        double s = Math.Sin(radAngle);
        double c = Math.Cos(radAngle);

        // translate point back to origin:
        pos.x -= center.transform.position.x;
        pos.z -= center.transform.position.z;

        // rotate point
        double xnew = pos.x * c - pos.z * s;
        double znew = pos.x * s + pos.z * c;

        pos.x = (float)(xnew + center.transform.position.x);
        pos.z = (float)(znew + center.transform.position.z);

        transform.position = pos;
        transform.Rotate(Vector3.up,-angle,Space.Self);
    }
}
