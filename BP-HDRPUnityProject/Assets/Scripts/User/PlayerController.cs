using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerController : MonoBehaviour
{
    public DeviceBasedContinuousMoveProvider provider;
    public GameObject boundingSphere;
    [Range(1, 5)]
    public float boundariesDistanceThreshold;

    private Renderer _boundingSphereRenderer;
    private SphereCollider _boundingSphereCollider;

    void Start()
    {
        _boundingSphereRenderer = boundingSphere.GetComponent<Renderer>();
        _boundingSphereCollider = boundingSphere.GetComponent<SphereCollider>();
    }

    void LateUpdate()
    {
        float bhDistance = transform.position.magnitude;
        float limit = boundingSphere.transform.localScale.x * _boundingSphereCollider.radius;
        if (bhDistance > limit)
        {
            transform.position = _boundingSphereCollider.ClosestPoint(transform.position);
        }

        _boundingSphereRenderer.material.shader = Shader.Find("Shader Graphs/BoundingBoxShader");
        if (limit-bhDistance < boundariesDistanceThreshold)
        {
            _boundingSphereRenderer.material.SetFloat("_Visibility", 1 - (limit - bhDistance)/ boundariesDistanceThreshold);
        }
        else
        {
            _boundingSphereRenderer.material.SetFloat("_Visibility", 0);
        }
        
    }
}
