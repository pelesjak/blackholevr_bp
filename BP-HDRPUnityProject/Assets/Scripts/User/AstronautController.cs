/**
 * @file AstronautController.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script for managing movement of user representation in the scene.
 */

using Photon.Pun;
using TMPro;
using UnityEngine;

public class AstronautController : MonoBehaviour
{
    public Vector3 controllersAngleOffset;

    public Transform MonoEye;
    public Transform HeadXR;
    public Transform LeftHandXR;
    public Transform RightHandXR;

    public GameObject Helmet;
    public GameObject Body;

    public GameObject DistanceWatch;
    public TMP_Text DistanceWatchText;
    public Gradient DistanceWatchGradient;
    public float MaxDistance;

    Animator animator;
    PhotonView photonView;
    Renderer DistanceWatchRenderer;

    Quaternion controllersQuaternionOffset { get { return Quaternion.Euler(controllersAngleOffset); } }

    void Start()
    {
        animator = GetComponent<Animator>();
        photonView = GetComponent<PhotonView>();
        if (!PhotonNetwork.IsConnected || photonView.IsMine)
        {
            Helmet.SetActive(false);
            Body.layer = LayerMask.NameToLayer("Camera Ignore 360");
        }

        DistanceWatchRenderer = DistanceWatch.GetComponent<Renderer>();
    }

    void Update()
    {
        float distance = transform.position.magnitude;
        DistanceWatchText.text = distance.ToString("N1");

        DistanceWatchRenderer.material.shader = Shader.Find("HDRP/Lit");
        DistanceWatchRenderer.material.SetColor("_BaseColor", DistanceWatchGradient.Evaluate(distance / MaxDistance));
        DistanceWatchRenderer.material.SetColor("_EmissiveColor", DistanceWatchGradient.Evaluate(distance / MaxDistance));
    }

    void OnAnimatorIK(int layerIndex)
    {
        float rightReach = animator.GetFloat("RightHandControl");
        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, rightReach);
        animator.SetIKPosition(AvatarIKGoal.RightHand, RightHandXR.position);

        animator.SetIKRotationWeight(AvatarIKGoal.RightHand, rightReach);
        animator.SetIKRotation(AvatarIKGoal.RightHand, RightHandXR.rotation * controllersQuaternionOffset);


        float leftReach = animator.GetFloat("LeftHandControl");
        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftReach);
        animator.SetIKPosition(AvatarIKGoal.LeftHand, LeftHandXR.position);

        animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftReach);
        animator.SetIKRotation(AvatarIKGoal.LeftHand, LeftHandXR.rotation * controllersQuaternionOffset);

        float lookReach = animator.GetFloat("LookControl");
        animator.SetLookAtWeight(lookReach);
        animator.SetLookAtPosition(HeadXR.position + HeadXR.forward * 10);

        Vector3 HeadOffset = MonoEye.position - animator.bodyPosition;
        animator.bodyPosition = HeadXR.position - HeadOffset;
    }
}
