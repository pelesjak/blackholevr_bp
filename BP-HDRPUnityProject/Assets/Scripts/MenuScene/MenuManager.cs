/**
 * @file MenuManager.cs
 * @author Jakub Pele�ka
 * @date 17 May 2021
 * @brief Script to handle main menu.
 */

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Launcher photonLauncher;
    public TMP_Text UserHeightText;
    public Slider UserHeightSlider;
    public SettingsData UserSettings;

    private void Start()
    {
        UserHeightSlider.value = UserSettings.UserHeight;
        UserHeightText.text = UserHeightSlider.value.ToString() + " cm";
    }

    /*
     * On enter main scene button.
     */
    public void OnPlay()
    {
        photonLauncher.Connect();
    }

    public void OnHeightChanged()
    {
        UserHeightText.text = UserHeightSlider.value.ToString() + " cm";
        UserSettings.UserHeight = (int)UserHeightSlider.value;
    }

    /*
     * On quit button.
     */
    public void OnQuit()
    {
        Application.Quit();
        Debug.Log("QUIT");
    }

    
}
