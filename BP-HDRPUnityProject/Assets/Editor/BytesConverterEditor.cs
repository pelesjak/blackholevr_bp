﻿/**
 * @file BytesConverterEditor.cs
 * @author Jakub Peleška
 * @date 17 May 2021
 * @brief Script to add custom UI to BytesConverter script.
 */

using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(BytesConverter))]
public class BytesConverterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BytesConverter convertor = (BytesConverter)target;
        if (GUILayout.Button("Convert bytes map to PNG"))
        {
            string path = EditorUtility.OpenFilePanel("Select file to convert", "", "bytes");
            if (path.Length != 0)
            {
                convertor.BytesMapToPNG(path);
            }
            
        }

        if (GUILayout.Button("Convert bytes map to EXR"))
        {
            string path = EditorUtility.OpenFilePanel("Select file to convert", "", "bytes");
            if (path.Length != 0)
            {
                convertor.BytesMapToEXR(path);
            }
        }

        if (GUILayout.Button("Convert all bytes maps in folder to EXR"))
        {
            string path = EditorUtility.OpenFolderPanel("Select folder", "", "");
            string[] files = Directory.GetFiles(path);

            foreach (string file in files)
            {
                if (file.EndsWith(".bytes"))
                    convertor.BytesMapToEXR(file);
            }
                
        }

        if (GUILayout.Button("Convert bytes data to EXR"))
        {
            string path = EditorUtility.OpenFilePanel("Select file to convert", "", "bytes");
            if (path.Length != 0)
            {
                convertor.BytesDataToEXR(path);
            }
        }

        if (GUILayout.Button("Convert bytes data to PNG"))
        {
            string path = EditorUtility.OpenFilePanel("Select file to convert", "", "bytes");
            if (path.Length != 0)
            {
                convertor.BytesDataToEXR(path);
            }
        }
    }
}
