﻿/**
 * @file BHManagerEditor.cs
 * @author Jakub Peleška
 * @date 17 May 2021
 * @brief Script to add custom UI to BHManager script.
 */

using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BHManager)), CanEditMultipleObjects]
public class BHManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BHManager manager = (BHManager)target;

        if (GUILayout.Button("Set data file"))
        {
            string path = EditorUtility.OpenFilePanel("Select file", "", "bytes");
            manager.dataPath = path;
        }
    }

    private Texture2D LoadImage(string filePath)
    {
        byte[] fileData;
        Texture2D tex = null;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }
        return tex;
    }
}
