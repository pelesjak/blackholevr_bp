# VR vizualizace černé díry s využitím sférické kamery

## Unity Project

 - nastavení projektu je připraveno pro použití Windows Mixed Reality headsetu


## Generování mapovacích dat - bhsim

 - Linux projekt
 - Pro použití dat ve VR je nutné přetvořit binární data do png formátu. K tomu slouží script Bytes2PNG v Unity projekt.

 ### 1. cíl
- [x] Přidat vzdálenost od BH na ruku
- [x] Plynulý pohyb lodě v prostoru
- [x] Bounding box
- [x] Rotace kolem středu scény
- [ ] Více dat pro černou díru bez rotace
- [ ] Dynamické FOV
- [ ] Optimalizace získávání 360 view
- [ ] Nastavení pravák/levák, výška uživatele


 ### 2. cíl
- [ ] Černá díra s rotací
- [ ] Test ray-traycing metody
- [ ] Zmenšená černá díra

