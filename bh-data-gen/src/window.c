/**
 * @file gravity.c
 * @author Jakub Peleška
 * @date 18 May 2021
 * @brief Script to create and control simple black hole visualization.
 *
 */


#include <stdio.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <fcntl.h>
#include <errno.h>
#include "gravity.h"

#define WIDTH 1280
#define HEIGHT 720
#define BPP 4
#define DEPTH 32


static SDL_Window* window = NULL;
static SDL_Renderer* renderer = NULL;
static SDL_Texture* texture = NULL;
static SDL_Surface* surface = NULL;

static int sizeX = 0;
static int sizeY = 0;

int bh_data_transform(uint32_t* pixels, gravity_data* data, SDL_Surface* surface, int sizex, int sizey, int R_index, int R_MAX_index, int FOV_deg);


void load(){
    surface = IMG_Load("test-img.jpg");
}


void window_open(int wX, int wY)
{
    sizeX = wX;
    sizeY = wY;
    
    SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

    // Create an application window with the following settings:
    window = SDL_CreateWindow(
        "An SDL2 window",                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        1.5*sizeX,                               // width, in pixels
        1.5*sizeY,                               // height, in pixels
        0//SDL_WINDOW_OPENGL                  // flags - see below
    );

    // Check that the window was successfully created
    if (window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        exit(1);
    }

    // Setup renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Could not create renderer: %s\n", SDL_GetError());
        exit(1);
    }
    
    // Set size of renderer to the same as window
    SDL_RenderSetLogicalSize(renderer, sizeX, sizeY);

    // Set color of renderer to green
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

    // Clear the window and make it all green
    SDL_RenderClear(renderer);

    SDL_RenderPresent(renderer);

    // create texture
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, sizeX, sizeY);

    load();
}


void window_close()
{
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}


void window_loop(gravity_data* gdata)
{
    int h =1;
    int fov = 55;
    int R_index = 0;
    int R_MAX_index = 0;

    uint32_t* pixels = (uint32_t*)malloc(sizeX*sizeY * sizeof(uint32_t));
    memset(pixels, 0x00000000, sizeX*sizeY * sizeof(uint32_t));

    int loop = 1;
    while (loop) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT)
                loop = 0;
            else if (event.type == SDL_KEYDOWN) {
                switch ( event.key.keysym.sym ) {
                    case SDLK_ESCAPE:
                        loop = 0;
                        break;
                    case SDLK_PAGEDOWN:
                        R_MAX_index = fmin(gdata->num_R-1, R_MAX_index+1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    case SDLK_PAGEUP:
                        R_MAX_index = fmax(0, R_MAX_index-1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    case SDLK_DOWN:
                        R_index = fmin(gdata->num_R-1, R_index+1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    case SDLK_UP:
                        R_index = fmax(0, R_index-1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    case SDLK_LEFT:
                        fov = fmin(90, fov-1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    case SDLK_RIGHT:
                        fov = fmax(0, fov+1);
                        memset(pixels, 0, sizeX*sizeY*sizeof(uint32_t));
                        break;
                    default :
                        break;
                }
                //printf("Zoom: %f, Angle: %f\n", zoom, angle);
                printf("R0: %f, R_MAX: %f, FOV: %d\n", gdata->Rs[R_index], gdata->Rs[R_MAX_index], fov*2);
            }
        }

        bh_data_transform(pixels, gdata, surface,sizeX, sizeY, R_index, R_MAX_index, fov);

        SDL_UpdateTexture(texture, NULL, pixels, sizeX*sizeof(uint32_t));
        SDL_RenderCopy(renderer, texture, NULL, NULL);
        // Render the changes above
        SDL_RenderPresent(renderer);
        // Add a 16msec delay to run at ~60 fps
        //SDL_Delay( 16 );
    }
    
    free(pixels);
}



