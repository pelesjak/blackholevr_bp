/**
 * @file main.c
 * @author Jakub Peleška
 * @date 18 May 2021
 * @brief Main file of program to generate black hole data with simple visualization.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <math.h>
#include "gravity.h"


void camera_open();
void camera_close();
void window_open(int sizeX, int sizeY);
void window_close();
void window_loop(gravity_data* gdata);

int size = 1440;
float range[] = {2.3, 3.33, 3.4, 3.5, 3.6, 3.7, 3.8, 4, 4.2, 4.4, 4.6, 4.8, 5, 5.4, 5.8, 6.2, 6.6, 7, 7.4, 7.8, 8.4, 9, 9.6, 10.2, 11, 12, 13, 14, 16, 18, 20};

void itoa(int value, char* buf, int base){
	int i = 30;
	buf = "";
	for(; value && i ; --i, value /= base) buf = "0123456789abcdef"[value % base] + buf;
}

float* logspace(float min, float max, int n, float range[])
{
    if(n < 2 || range == 0)
        return (void*)0;

    float logarithmicBase = M_E;
    float logMin = log(min);
    float logMax = log(max);
    float delta = (logMax - logMin) / (float)(n-1);
    float accDelta = 0;
    for (int i = 0; i < n-1; ++i){
        range[i] = (float) pow(logarithmicBase, logMin + accDelta);
        printf("range - %d = %f\n", i+1, range[i]);
        accDelta += delta;// accDelta = delta * i
    }
    range[n-1] = max;
    printf("range - %d = %f\n", n, range[n-1]);
    return range;
}

int main(int argc, char* argv[])
{
    gravity_data gdata = {0};
    char cachefile[64];

    if (argc < 2)
        sprintf(cachefile, "bhvis.profile");
    else
        strncpy(cachefile, argv[1], sizeof(cachefile));

    if (access(cachefile, F_OK) != -1 && !opendir(cachefile)) {
        
        gravity_data_load(&gdata, cachefile);
        size = gdata.length;
        for(int j = 0; j < gdata.num_R; j++){
            for(int i = 0; i < size; i++){
                printf("R: %f, phi: %f\n", gdata.Rs[j], gdata.data[j*size + i].phi);
            }
        }
    } else {

        if(argc >= 3){
            size=atoi(argv[2]);  
        }

        fprintf(stderr, "Computing transformation ...\n");

        float t_range[1000];
        int length = sizeof(t_range)/sizeof(float);
        logspace(2.02,30,length, t_range);

        // Edit here to make more data!
        gravity_data_alloc(&gdata, size, t_range, length);

        printf("Generating data with...\n");
        // gravity_schwarzschild_bh(&gdata, 2.02, 40, 0);
        #pragma omp parallel for
        for(int i = 0; i < gdata.num_R; i++){
            gravity_schwarzschild_bh(&gdata, gdata.Rs[i], 40, i);
        }
    
        if(argc > 3 && !strcmp(argv[3],"save")){
            strcat(cachefile, ".bytes");
            gravity_data_save(&gdata,cachefile);
        }
    }

    window_open(size*2, size*2);

    window_loop(&gdata);

    window_close();

    gravity_data_free(&gdata);
    return 0;
}


