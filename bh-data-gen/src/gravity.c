/**
 * @file gravity.c
 * @author Jakub Peleška
 * @date 18 May 2021
 * @brief Definitions of functions needed to generate black hole distortion data.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sim5lib.h"
#include "gravity.h"

void gravity_data_alloc(gravity_data* data, int r, float* range, int num_R)
{
    data->length = r;
    data->Rs = range;
    data->num_R = num_R;
    int size = data->length*data->num_R;
    data->data = calloc(size, sizeof(gravity_data_pixel));
    fprintf(stderr,"alloc %ld bytes\n", size*sizeof(gravity_data_pixel));
}

void gravity_data_free(gravity_data* data)
{
    data->length = 0;
    data->Rs = NULL;
    data->num_R = 0;
    free(data->data);
}

void gravity_data_clear(gravity_data* data)
{
    int size = data->length*data->num_R;
    memset(data->data, 0, size*sizeof(gravity_data_pixel));
}


void gravity_data_load(gravity_data* data, const char* filename)
{
    int length, num_R;
    FILE* f;
    f = fopen(filename, "rb");
    fread(&length,  sizeof(length), 1, f);
    fread(&num_R, sizeof(num_R), 1, f);
    data->length = length;
    data->num_R = num_R;
    data->Rs = calloc(num_R, sizeof(float));
    data->data = realloc(data->data, length*num_R*sizeof(gravity_data_pixel));
    gravity_data_clear(data);
    fread(data->Rs, sizeof(float), num_R, f);
    fprintf(stderr,"loading %ld bytes\n", sizeof(gravity_data_pixel)*length*num_R);
    fread(data->data, sizeof(gravity_data_pixel), length*num_R, f);
    fclose(f);
}

void gravity_data_save(gravity_data* data, const char* filename)
{
    FILE* f;
    f = fopen(filename, "w");
    fwrite(&data->length,  sizeof(data->length),  1, f);
    fwrite(&data->num_R, sizeof(data->num_R), 1, f);
    fwrite(data->Rs, sizeof(float), data->num_R, f);
    fwrite(data->data, sizeof(gravity_data_pixel), data->length*data->num_R, f);
    fprintf(stderr,"saving %ld bytes, filename: %s\n", sizeof(gravity_data_pixel)*data->length*data->num_R, filename);
    fflush(f);
    fclose(f);
}

void __momentum_inversion(double k[4], double k_inv[4], sim5tetrad* tetrad) {
    double n[4];
    bl2on(k, n, tetrad);
    n[1] = -n[1];
    n[2] = -n[2];
    n[3] = -n[3];
    on2bl(n, k_inv, tetrad);
}


void gravity_schwarzschild_bh(gravity_data* data, float r0, float R_MAX, int data_offset)
{
    float precision = 0.000001f;
    float bh_spin = 0;
    float m0 = 0;

    float R_MIN = 1.01*r_bh(bh_spin);
    int R_EVH = -1;
    int R_INF = -2;
    int R_ERR = -3;

    double max_angle = M_PI; // Field of view //asin((double)R_MAX/(double)r0);

    sim5metric metric_local;
    sim5tetrad tetrad_local;
    sim5metric metric_remote;
    sim5tetrad tetrad_remote;
    double photon_n[4];
    double photon_k[4];
    double photon_x[4];
    double dl;
    raytrace_data rtd;
    
    // metric and tetrad for local point
    kerr_metric(bh_spin, r0, m0, &metric_local);
    tetrad_zamo(&metric_local, &tetrad_local);
    
    for (int i=0; i<data->length; i++) {

        gravity_data_pixel* pix = &data->data[data_offset*data->length + i];
        pix->phi = 0.0;
        pix->g = 0.0;

        double angle = max_angle*(1 - ((double)i)/((double)data->length-1));

        // define unit vector pointing in the direction of incoming photon for current pixel
        // (for simplicity assume that the line of sight is along the radius vector)
        photon_n[0] = 1.0;
        photon_n[1] = -r0 / (r0/ cos(angle));
        photon_n[2] = 0;
        photon_n[3] = (r0*tan(angle)) / (r0/ cos(angle));

        // place of emission
        photon_x[0] = 0.0;
        photon_x[1] = r0;
        photon_x[2] = m0;
        photon_x[3] = 0.0;

        // convert local momentum vector to BL coordinate vector
        on2bl(photon_n, photon_k, &tetrad_local);
        // printf("k: (%f, %f, %f, %f)\n", photon_k[0], photon_k[1], photon_k[2], photon_k[3]);
        
        // get energy shift (g-factor) with respect to infinity of photon that is INCOMING to the local position
        // note: since we raytrace backwards, we need to make the inversion of the momentum direction
        //note: 4-velocity is the first base vector of the tetrad and (1,0,0,0)*e[\mu][(a)] = U^\mu
        double* U_local = tetrad_local.e[0];
        double k_local[4];
        __momentum_inversion(photon_k, k_local, &tetrad_remote);
        float gfactor_local = (k_local[0]*metric_local.g00 + k_local[3]*metric_local.g03) / dotprod(k_local, U_local, &metric_local);

        // prepare for raytracing
        raytrace_prepare(bh_spin, photon_x, photon_k, NULL, precision, 0, &rtd);

        // photon raytracing 
        int rt_res = 0;
        while (rt_res == 0) {
            dl = 0.1;
            raytrace(photon_x, photon_k, NULL, &dl, &rtd);

            if (rtd.error > 0.1) rt_res = R_ERR;
            if (photon_x[1] < R_MIN) rt_res = R_EVH;
            if (isnan(dl)) rt_res = R_ERR;

            if (photon_x[1] > R_MAX) {
                rt_res = R_INF; 
            }
        }//while

        // printf("exit code: %d\n", rt_res);

        if (rt_res == R_INF) {
            double y = photon_x[1] * sin(photon_x[3]-M_PI);
            double x = photon_x[1] * cos(photon_x[3]-M_PI) + r0;
            float finalAngle = -atan2(y,x);
            // printf("start_angle: %.2f, end_angle: %.2f\n", angle*180 / M_PI, finalAngle *180 / M_PI);
            
            pix->phi = finalAngle;
            pix->g = 1.0;
        }
    }
}