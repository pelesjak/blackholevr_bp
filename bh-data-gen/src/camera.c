/**
 * @file camera.c
 * @author Jakub Peleška
 * @date 18 May 2021
 * @brief Function file for simple black hole visualization.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <sys/mman.h>
#include <SDL2/SDL_image.h>
#include "gravity.h"


int bh_data_transform(uint32_t* pixels, gravity_data* data, SDL_Surface* surface, int sizex, int sizey, int R_index, int R_MAX_index, int FOV_deg)
{
    float max_phi = -100000, min_phi = 100000;
    int x,y;
    double FOV = FOV_deg *M_PI / 180.0;
    double alpha = 1/tan(FOV);
	for (y=0;y<surface->h;y++) {
		for (x=0;x<surface->w;x++) {
		    if ((x < sizex) && (y <sizey)) {
		        uint32_t vv;
		        uint8_t* pp = (void*)&vv;

                int x0 = x-(data->length);
                int y0 = y-(data->length);
                double r0 = sqrt(x0*x0+y0*y0);

                int index = round((data->length)-r0);
                index = index >= data->length ? data->length-1 : (index < 0 ? 0 : index);

                int offset = R_index * data->num_R * data->length + R_MAX_index*data->length;
                
                gravity_data_pixel* pix = &data->data[offset+index];

                double beta = alpha / cos(asin(pix->phi));
                double r = sqrt(beta*beta - alpha*alpha);
                
                double u = (double)(-x0/r0)*r;
                double v = (double)(-y0/r0)*r;

                if ((u>1.0) || (u<-1.0) || (v>1.0) || (v<-1.0) || (pix->g==0.0)) continue;
                int iu = (int)((u/2.+0.5)*sizex);
                int iv = (int)((v/2.+0.5)*sizey);
                if ((iu<0)||(iu>=surface->w)) continue;
                if ((iv<0)||(iv>=surface->h)) continue;

		        uint8_t* src = surface->pixels + (iv*surface->w+iu)*3;

		        pp[0]=src[2];    // blue
		        pp[1]=src[1];    // green
		        pp[2]=src[0];    // red
		        pp[3]=0x00;      // alpha
		        
    		    pixels[y*sizex+x] = vv;
		    }
		    
        }
    }
}

