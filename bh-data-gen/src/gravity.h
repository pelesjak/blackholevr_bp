/**
 * @file gravity.h
 * @author Jakub Peleška
 * @date 18 May 2021
 * @brief Header file with declarations of functions needed to generate black hole distortion data.
 *
 */

#ifndef _GRAVITY_H
#define _GRAVITY_H

/*
 * Data structure.
 */
typedef struct gravity_data_pixel {
    float phi;
    float g;
} gravity_data_pixel;

typedef struct gravity_data {
    int length;
    float* Rs;
    int num_R;
    gravity_data_pixel* data;
} gravity_data;

#define gravity_map_pix(map,x,y) map->map[y*map->width+x]

/*
 * Allocate data structure.
 */
void gravity_data_alloc(gravity_data* data, int r, float* range, int num_R);

/*
 * Free data structure.
 */
void gravity_data_free(gravity_data* data);

/*
 * Clear data structure.
 */
void gravity_data_clear(gravity_data* data);

/*
 * Load already generated data structure.
 */
void gravity_data_load(gravity_data* data, const char* filename);

/*
 * SaveLoad already generated data structure.
 */
void gravity_data_save(gravity_data* data, const char* filename);

/*
 * Generate black hole distortion data.
 */
void gravity_schwarzschild_bh(gravity_data* data, float r0, float R_MAX, int data_offset);

#endif
