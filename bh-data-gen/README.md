# Simple interactive visualization of Schwarzschild black hole
The program generates data for distortion of images by a black hole effect.
A simple visualization of the distortion effect is implemented within the app.
## Requirements
 - Linux only
 - install SDL2 library -> `sudo apt-get install libsdl2-dev`
 - install SDL2_image library -> `sudo apt-get install libsdl-image1.2-dev`

## Run setup
 - `make`
 - to create new data: `./bh-data-gen <profile_name> <data_radius_in_pixels> save` 
 - to visualize already generated data: `./bh-data-gen <name_of_file_with_generated_data>`

## Controls of simple visualization
 - Arrow left -> lower FOV
 - Arrow right -> higher FOV
 - Arrow up -> higher R0
 - Arrow down -> lower R0
 - Page up -> higher R_MAX
 - Page down -> lower R_MAX
